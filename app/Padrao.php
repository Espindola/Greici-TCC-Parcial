<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\belongsToMany;
use DB;

class Padrao extends Model
{
    protected $table = 'padrao_template';
    public $timestamps = false;
    protected $fillable = array('nome', 'tamanho_largura', 'tamanho_altura', 'num_max_fonte', 'tamanho_min_font', 'tamanho_max_font', 'contem');
    protected $guarded = ['id_padrao'];

    public function leis()
    {
        return $this->belongsToMany('App\Lei', 'template_lei','id_padrao','id_lei');
    }

    public function listaTemplateLei($id)
    {
        $associacao = DB::table('template_lei')->where('id_template', '=', $id)->get();

        return $associacao;
    }
}
