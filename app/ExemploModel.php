<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use DB;
use Route;
use Auth;
use App\Papel;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'cpf', 'activated'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function papeis()
    {
        return $this->belongsToMany('App\Papel', 'id');
    }

    public function tratamentos()
    {
        return $this->hasMany('App\Tratamento', 'id_pcd');
    }


    public function listaNaoAutorizados()
    {
        $naoAutorizados = DB::table('users')->where('activated', 0)->get();

        return $naoAutorizados;

    }


    public function rejeita($id)
    {
        DB::table('users')->where('id', '=', $id)->delete();

    }


    public function aceita($id)
    {
        DB::table('users')
            ->where('id', $id)
            ->update(['activated' => 1]);

        $users = DB::table('users')->where('id', $id)->get();

        return $users;

    }

    public function atribuiPapel($descricao, $id)
    {

        $papel = DB::table('papel')->where('descricao', '=', $descricao)->first();


        DB::table('usuario_papel')->insert(
            ['user_id' => $id, 'papel_id' => $papel -> papel_id]);

    }


    public function lista()
    {
        $users = DB::table('users')->where('activated', '=', 1)->get();

        return $users;
    }


    public function exclui($id)
    {
        DB::table('users')->where('id', '=', $id)->delete();

    }

    public function edita($id)
    {
        $user = User::find($id);

        if(is_null($user)){
            return Redirect::route('/User/lista');
        }

        $usuario_papel = DB::table('usuario_papel')->where('user_id', '=', $id)->first();

        $papel = DB::table('papel')->where('papel_id', '=', $usuario_papel -> papel_id)->first();

        return $papel;
    }

    public function edita2($id, $descricao)
    {

        $papel = DB::table('papel')->where('descricao', '=', $descricao)->first();


        DB::table('usuario_papel')
            ->where('user_id', $id)
            ->update(['papel_id' => $papel -> papel_id]);
    }

    public function verificaPermissao(){

        if (Auth::check()){
            $user_id = Auth::user()->id;
        }

        $permissao = DB::table('papel')
            ->select('usuario_papel.papel_id')
            ->join('usuario_papel','usuario_papel.papel_id', '=', 'papel.papel_id')
            ->join('users','users.id', '=', 'usuario_papel.user_id')
            ->where('usuario_papel.user_id', '=', $user_id)
            ->get();

        $arrayPermissao = array();

        foreach ($permissao as $key => $value) {
            $arrayPermissao[$key] = $permissao[$key]->papel_id;
        }

        return $arrayPermissao;
    }







}
