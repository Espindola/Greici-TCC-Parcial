<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/per', function () {
    return view('perfil');
});

Route::get('/ll', function () {
    return view('Administrador/cadastraPadrao');
});

Route::get('/logout', function () {
    return view('welcome');
});

// Route::get('/desenho', function () {
//     return view('desenho');
// });

/* PADROES */
Route::get('/Padrao/novo', 'PadraoController@novo');
Route::post('/Padrao/adiciona', 'PadraoController@adiciona');
Route::get('/Padrao/lista', 'PadraoController@lista');
Route::get('/Padrao/detalhes{id}', 'PadraoController@mostra');
Route::get('/Padrao/edita/{id}', 'PadraoController@edita');
Route::get('/Padrao/exclui/{id}', 'PadraoController@exclui');

/* DICAS*/
Route::get('/Dica/novo', 'DicaController@new');
Route::post('/Dica/adiciona', 'DicaController@adiciona');
Route::get('/Dica/lista', 'DicaController@lista');
Route::get('/Dica/detalhes{id}', 'DicaController@mostra');
Route::get('/Dica/edita/{id}', 'DicaController@edita');
Route::get('/Dica/atualiza/{id}', 'DicaController@atualiza');
Route::get('/Dica/exclui/{id}', 'DicaController@exclui');

/* LEIS*/
Route::get('/Lei/novo', 'LeiController@novo');
Route::post('/Lei/adiciona', 'LeiController@adiciona');
Route::get('/Lei/lista', 'LeiController@lista');
Route::get('/Lei/detalhes{id}', 'LeiController@mostra');
Route::get('/Lei/edita/{id}', 'LeiController@edita');
Route::get('/Lei/exclui/{id}', 'LeiController@exclui');

/* DESENHO*/
Route::get('/Desenho/novo', 'DesenhoController@new');
Route::get('/Desenho/criar', 'DesenhoController@criar');
Route::get('/Desenho/criarL', 'DesenhoController@criarL');

Route::get('/Perfil', 'PerfilController@perfil');
/* USUARIO*/
Route::get('/Usuario/lista', 'UserController@lista');
Route::get('/Usuario/edita/{id}', 'UserController@edita');
Route::get('/Usuario/exclui/{id}', 'UserController@exclui');

// Route::get('/perfil', function () {
//     return view('perfil');
// });

// Route::get('/lista', 'UserController@lista');
Route::get('/des', function () {
    return view('/User/desenho');
});

Route::get('/login-unauthorized', function () {
    return view('authRequired');
});
