<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Route;
use Auth;
use App\Papel;


class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }



    public function listaNaoAutorizados()
    {
        $user = new User();
        $naoAutorizados = $user->listaNaoAutorizados();

        return view('/User/solicitacoes')->with('naoAutorizados', $naoAutorizados);
    }


    public function rejeita($id)
    {
        $user = new User();
        $user->rejeita($id);

        return redirect()->action('UserController@listaNaoAutorizados');
    }


    public function aceita($id)
    {
        $user = new User();
        $users = $user->aceita($id);

        return view('/User/atribuiPapel')->with('users', $users);
    }

    public function atribuiPapel(Request $request, $id)
    {
        $descricao = $request->input('descricao');
        $user = new User();

        $user->atribuiPapel($descricao, $id);

        return view('/User/sucessoSolicitacoes');
    }



    public function lista()
    {
        $user = new User();
        $users = $user->lista();
        return view('/User/lista')->with('users', $users);
    }


    public function exclui($id)
    {
        $user = new User();

        $user->exclui($id);
        return redirect()->action('UserController@lista');
    }

    public function edita($id)
    {
        $user = new User();

        $papel = $user->edita($id);
        $user = User::find($id);

        return \View::make('/User/edita', ['user' => $user, 'papel' => $papel]);
    }

    public function edita2(Request $input, $id)
    {
        $user = new User();

        $user = User::find($id);
        $user -> name = $input -> Input('name');
        $user -> email = $input -> Input('email');
        $user -> cpf = $input -> Input('cpf');
        $user -> save();

        $descricao = $input->Input('descricao');

        $user->edita2($id, $descricao);

        return view('/User/sucessoUsuarios');
    }


    public function verificaPermissao(){
        $user = new User();

        $arrayPermissao = $user->verificaPermissao();

        return $arrayPermissao;
    }


}
