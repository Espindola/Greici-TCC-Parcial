<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Padrao;
use App\Lei;
use App\User;
use Illuminate\Http\Request;
// use App\Http\Requests\PCDRequest;
// use DB;

class PadraoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function novo() {
      $leis = Lei::all();
      return view('Padrao/cadastro')->with('leis', $leis);
    }

    public function adiciona(Request $request) {


      // //Padrão
        $nome = $request->input('nome');
        $alt = $request->input('tm-altura');
        $larg = $request->input('tm-largura');
        $numFontMax = $request->input('num-max-font');
        $tmFontMin = $request->input('tm-min-font');
        $tmMaxFont = $request->input('tm-max-font');
        $contem = $request->input('contem');
        $leis = $request->input('leis');

        $padrao = new Padrao;
        $padrao->nome = $nome;
        $padrao->tamanho_largura = ($alt * 36);
        $padrao->tamanho_altura = ($larg*36);
        $padrao->num_max_fonte = $numFontMax;
        $padrao->tamanho_min_font = $tmFontMin;
        $padrao->tamanho_max_font = $tmMaxFont;
        $padrao->contem = $contem;
        $padrao->save();


        for ($i = 0; $i < count($leis); $i++){
          DB::table('template_lei')->insert(
                ['id_padrao' => $padrao->id, 'id_lei' => $leis[$i]]);
        }

       return view('/Padrao/sucesso');
    }




   public function lista(){
       $pads = Padrao::all();
       return view('Padrao/lista') ->with('pads', $pads);
    }


      public function edita($id)
    {
        $pad = Padrao::find($id);
        if(is_null($pad)){
            return Redirect::route('/Padrao/lista');
        }

       // MUDAAAAAAAARRRRRRR
      //   $endereco = DB::table('endereco')->where('id', '=', $pcd->id_endereco)->first();
      //
      //    $contato = DB::table('contato')->where('id_pcd', '=', $pcd->id)->first();
      //
      //   $tratamentos = DB::table('tratamento')->where('id_pcd', '=', $pcd->id)->get();
      //
      //   $adicionais = DB::table('adicional')->where('id_pcd', '=', $pcd->id)->get();
      //
      //  return \View::make('/PCD/edita', ['pcd' => $pcd, 'endereco' => $endereco, 'contato' => $contato, 'tratamentos' => $tratamentos,  'adicionais' => $adicionais]);

    }

    public function escolhe($id){
      $pad = Padrao::find($id);
      $padsAs = $pad->listaTemplateLei($pad->id);
      $leis = Leis::All();
      $dicas = Dica::All();

      return view('User/desenho') ->with('pad', $pad, 'leisAssociadas',$padsAs, 'leis', $leis, 'dicas', $dicas);
    }

}
