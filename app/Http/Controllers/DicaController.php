<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Dica;
use Illuminate\Http\Request;

class DicaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function new() {
        return view('Dica/cadastro');
    }
    public function novo() {
        return view('Dica/cadastro');
    }

    public function adiciona(Request $request) {

        //Dica
        $nome = $request->input('nome');
        $descricao = $request->input('descricao');

        $dica = new Dica;
        $dica->nome = $nome;
        $dica->descricao = $descricao;
        $dica->save();
       return view('/Dica/lista');

    }

   public function lista(){
       $dicas = Dica::all();
       return view('Dica/lista') ->with('dicas', $dicas);
    }

    public function exclui($id)
    {
        $dica = new Dica;

        $dica->exclui($id);
        return redirect()->action('DicaController@lista');
    }

    public function edita($id)
    {
        // $user = new User();
        //
        // $papel = $user->edita($id);
        $dica = Dica::find($id);

        return redirect()->action('/Dica/edita')->with('dica', $dica);;
    }

    public function atualiza(Request $request,$id)
    {
        // $user = new User();
        //
        // $papel = $user->edita($id);
        $dica = Dica::find($id);
        $descricao = $input -> Input('descricao');
        $nome = $input -> Input('nome');
        // $user -> save();
        //
        // $descricao = $input->Input('descricao');

        $user->edita2($id, $nome,$descricao);

        return view('Dica/sucesso');
        //
        // return redirect()->action('Dica/edita')->with('dicas', $dicas);;
    }

    // public function edita($id){
    //   $dica = Dica::find($id);
    //   if(is_null($dica)){
    //       return Redirect::route('/Dica/lista');
    //   }
    //   return Redirect::route('/Dica/edita');
    // }

  }


/*
    public function mostra($id){
        $pcd = PCD::find($id);
             if(empty($pcd)) {
                 return "Esse paciente não existe";
            }
        return view('PCD/detalhes')
        ->with('pcd', $pcd);
    }

    public function listaJson(){
        $pcds = PCD::all();
        return response()->json($pcds);
    }

  */
