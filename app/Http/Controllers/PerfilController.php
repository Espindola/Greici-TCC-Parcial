<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Padrao;
use App\Lei;
use App\User;
use Illuminate\Http\Request;
// use App\Http\Requests\PCDRequest;

class PerfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function perfil() {
      return view('User/perfil');
    }
}
