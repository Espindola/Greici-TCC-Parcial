<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Desenho;
use App\Padrao;
use App\PadraoLei;
use App\Lei;
use App\Dica;
use Illuminate\Http\Request;
// use DB;

class DesenhoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function new() {
      $pad = Padrao::all();
        return view('Desenho/escolha')->with('pads', $pad);;
    }

    public function criar(Request $request) {
        $id = $request->input('padroes');

            $pad  = DB::table('padrao_template')->where('id_padrao', '=', $id)->get();
            $padsAs = PadraoLei::All();
            $leis = Lei::All();
            $dicas = Dica::All();
            // dd($dicas);
            return view('Desenho/desenho') ->with(['pad'=>$pad,'leisAssociadas'=>$padsAs,'leis'=>$leis, 'dicas'=>$dicas]);

}

public function criarL() {

        $padsAs = PadraoLei::All();
        $leis = Lei::All();
        $dicas = Dica::All();
        // dd($dicas);
        return view('Desenho/desenhoLivre') ->with(['leisAssociadas'=>$padsAs,'leis'=>$leis, 'dicas'=>$dicas]);

}
    }
