<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Lei;
use Illuminate\Http\Request;


class LeiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function novo() {
        return view('Lei/cadastro');
    }

    public function adiciona(Request $request) {

        //Dica
        $nome = $request->input('nome');
        $descricao = $request->input('descricao');

        $lei = new Lei;
        $lei->nome = $nome;
        $lei->descricao = $descricao;
        $lei->save();

        $leis =Lei::all();
        return view('Lei/lista') ->with('leis', $leis);

    }

   public function lista(){
       $leis =Lei::all();
       return view('Lei/lista') ->with('leis', $leis);
    }


    public function edita($id){
      $lei = Lei::find($id);
      if(is_null($lei)){
          return Redirect::route('/Lei/lista');
      }
      // $dica1 = DB::table('dica')->where('id_dica', '=', $dica->id_dica)->first();
      // // $contato = DB::table('contato')->where('id_pcd', '=', $pcd->id)->first();
      // // $tratamentos = DB::table('tratamento')->where('id_pcd', '=', $pcd->id)->get();
      // // $adicionais = DB::table('adicional')->where('id_pcd', '=', $pcd->id)->get();

     return \View::make('/Lei/edita') ->with('leis', $leis);;

  }

    public function atualiza($id){
      $lei = Lei::find($id);
      if(is_null($lei)){
          return Redirect::route('/Lei/lista');
      }
    }
/*
    public function mostra($id){
        $pcd = PCD::find($id);
             if(empty($pcd)) {
                 return "Esse paciente não existe";
            }
        return view('PCD/detalhes')
        ->with('pcd', $pcd);
    }

    public function listaJson(){
        $pcds = PCD::all();
        return response()->json($pcds);
    }

  */

}
