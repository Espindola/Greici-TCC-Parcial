<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\belongsToMany;
use DB;

class PadraoLei extends Model
{
    protected $table = 'template_lei';
    public $timestamps = false;
    protected $fillable = array('id_padrao', 'id_lei');
    protected $guarded = ['id'];


}
