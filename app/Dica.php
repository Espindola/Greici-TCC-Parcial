<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use DB;

class Dica extends Model
{
    protected $table = 'dica';
    public $timestamps = false;
    protected $fillable = array('nome', 'descricao');
    protected $guarded = ['id_dica'];

    public function exclui($id)
    {
        DB::table('dica')->where('id_dica', '=', $id)->delete();

    }
    // public function pads()
    // {
    //     return $this->hasMany('App\Padrao', 'id_lei');
    // }
    public function edita($id, $nome, $descricao)
    {

        // $papel = DB::table('dica')->where('id_dica', '=', $id)->first();


        DB::table('dica')
            ->where('id_dica', $id)
            ->update(['nome' => $nome, 'descricao' => $descricao]);
    }
}
