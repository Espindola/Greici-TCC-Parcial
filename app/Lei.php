<?php

namespace App;

// use App\Models\Padrao;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use DB;

class Lei extends Model
{
    protected $table = 'lei';
    public $timestamps = false;
    protected $fillable = array('nome', 'descrição');
    protected $guarded = ['id_lei'];

    public function pads()
    {
        return $this->belongsToMany('App\Padrao','id_lei','id_padrao');
    }
}
