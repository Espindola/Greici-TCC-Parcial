@extends('layouts.app')
@section('content')
<!-- <body class="qualquer"> -->
<body background="/img/10.jpg">
  <div class="parallax-container-my">
    <!-- <div class="parallax"><img src="/img/10.jpg"> -->
      <br>
        <div class="container">
            <div class="row">

                <div class="col s12 m8 20 offset-m2 offset-2">
                    <div class="card card-center white">
                        <div class="card-content black-text">@if($lei != null)
                            <div class="center">
                                <span class="card-title">Lei {{ $lei->nome }}</span>
                            </div>
                            <div class="center">
                                <span class="card-title">Lei {{ $lei->descricao }}</span>
                            </div>

                            <div class="card-action">
                              <div class="center">
                                                <button type="submit" class="btn  grey darken-1 largura">
                                    <i class="fa fa-btn fa-sign-in"></i> Cadastrar
                                </button>
                                  <a class="btn-flat btn largura" href="{{ url('/') }}">Cancelar</a>
                              </div>
                            </div>
                            @else

                                    <div class="icon-block">
                                      <h2 class="center grey-text"><i class="material-icons">not_interested</i></h2>
                                      <h5 class="center">Lei não encontrada.</h5>
                                    </div>@endif

                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- </body> -->

@endsection
