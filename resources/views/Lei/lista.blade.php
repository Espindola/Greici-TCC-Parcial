@extends('layouts.app')
@section('content')
<!-- <body class="qualquer"> -->
<body background="/img/10.jpg">
  <div class="parallax-container-my">
    <!-- <div class="parallax"><img src="/img/10.jpg"> -->
      <br>
        <div class="container">
            <div class="row">

                <div class="col s12 m8 20 offset-m2 offset-2">
                    <div class="card card-center white">
                        <div class="card-content black-text">
                            <div class="center">
                                <span class="card-title">Dicas para Designer!</span>
                            </div>

                            <div class="col-md-12">@if($leis != null)
                              <table>
                                      <thead>
                                        <tr>
                                            <th> Dica </th>
                                            <th> Ação </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      @foreach ($leis as $lei)
                                        <tr>
                                          <td>{{ $lei->nome }}</td>
                                          <td>

                                            <a href="{{action('LeiController@mostra', $lei->id)}}" class="btn-floating btn-medium green"> <i class="large material-icons"> zoom_in </i></a>
                                            <a href="{{action('LeiController@edita', $lei->id)}}" class="btn-floating btn-medium blue"> <i class="large material-icons"> mode_edit </i></a>
                                            <a href="{{action('LeiController@exclui', $lei->id)}}" class="btn-floating btn-medium red"> <i class="large material-icons"> delete </i></a>

                                          </td>
                                        </tr>
                                      @endforeach
                                      </tbody>
                                    </table>@else

                                    <div class="icon-block">
                                      <h2 class="center grey-text"><i class="material-icons">not_interested</i></h2>
                                      <h5 class="center">Nenhuma Dica cadastrada.</h5>
                                    </div>@endif
                                    <a class="btn-flat btn largura, rigth" href="{{action('PerfilController@perfil')}}">Cancelar</a>


                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- </body> -->

@endsection
