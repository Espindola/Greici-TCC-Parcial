@extends('layouts.app')

@section('content')

<body background="/img/10.jpg" class="semRotacaoLateral">
<!-- <div class="parallax-container-my">
  <div class="parallax"><img src="/img/10.jpg"></div>
<br> -->
<div class="container">
    <div class="row">

        <div class="col s12 m8 l6 offset-m2 offset-l3">
            <div class="card card-center white">
                <div class="card-content black-text">
                    <div class="center">
                        <span class="card-title">Cadastre Seu Template!</span>
                    </div>

                    <form class="form-horizontal" role="form" action="{{action('PadraoController@adiciona')}}"  method="post">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">

                      <!-- Nome -->
                      <label for="nome" class="col-md-4 control-label">Nome do Padrão</label>
                      <div class="col-md-6">
                          <input id="nome" type="text" class="form-control" name="nome">
                      </div>

                      <!-- Largura  -->
                      <label for="tmLargura" class="col-md-4 control-label">Tamanho Largura (cm)</label>
                      <div class="col-md-6">
                              <input id="tmLargura" type="text" class="form-control" name="tm-largura">
                      </div>

                      <!-- Altura  -->
                            <label for="tmAltura" class="col-md-4 control-label">Tamanho Altura(cm)</label>
                            <div class="col-md-6">
                                <input id="tmAltura" type="text" class="form-control" name="tm-altura">
                            </div>

                        <!-- Num max fonte  -->
                            <label for="fontMax" class="col-md-4 control-label">Número Máximo de Fontes</label>
                            <div class="col-md-6">
                                <input id="fontMax" type="number" class="form-control" name="num-max-font">
                            </div>

                          <!-- Tm min fonte  -->
                            <label for="fontMin" class="col-md-4 control-label">Tamanho Minimo da Fonte</label>
                            <div class="col-md-6">
                                <input id="fontMin" type="number" class="form-control" name="tm-min-font">
                            </div>

                      <!-- tM max fonte  -->
                            <label for="fontMin" class="col-md-4 control-label">Tamanho Maximo da Fonte</label>
                            <div class="col-md-6">
                                <input id="fontMin" type="number" class="form-control" name="tm-max-font">
                            </div>

                            <div class="row">
                               <div class="input-field col s12">
                                  <textarea id="contem" name="contem" class="materialize-textarea"></textarea>
                                  <label for="contem">Padrão deve conter: (separe por ;)</label>
                               </div>
                            </div>

                        <p> Leis:</p>
                        @if($leis != null)
                        @foreach ($leis as $lei)
                        <p>
                          <input type="checkbox" id="{{ $lei->id }}" />
                        <label for="{{ $lei->id }}" name="{{ $lei->id }}">{{ $lei->nome }} </label>
                        </p>
                        @endforeach
                        @else
                        <div class="icon-block">
                          <h2 class="center grey-text"><i class="material-icons">not_interested</i></h2>
                          <h5 class="center">Nenhuma Lei cadastrada.</h5>
                        </div>@endif


                        <div class="card-action">
                          <div class="center">
                                            <button type="submit" class="btn  grey darken-1 largura">
                                <i class="fa fa-btn fa-sign-in"></i> Cadastrar
                            </button>
                              <a class="btn-flat btn largura" href="{{ url('/') }}">Cancelar</a>
                          </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- </body> -->

@endsection
