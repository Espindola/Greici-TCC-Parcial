@extends('layouts.app')
@section('content')
 <body background="/img/10.jpg" >
  <div  class="parallax-container-my">
    <!-- <div class="parallax"><img src="/img/10.jpg"> -->
      <br>
        <div class="container">
            <div class="row">

                <div class="col s12 m8 20 offset-m2 offset-2">
                    <div class="card card-center white">
                        <div class="card-content black-text">
                            <div class="center">
                                <span class="card-title">Padrões</span>
                            </div>

                            <div class="col-md-12">@if($pads != null)
                              <table>
                                      <thead>
                                        <tr>
                                            <th> Nome </th>
                                            <th> Lag </th>
                                            <th> Alt </th>
                                            <th> Ações </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      @foreach ($pads as $pad)
                                        <tr>
                                          <td>{{ $pad->nome }}</td>
                                          <td>{{ $pad->tamanho_largura }}</td>
                                          <td>{{ $pad->tamanho_altura }}</td>
                                          <td>
                                            <a href="{{action('PadraoController@edita', $pad->id)}}" class="btn-floating btn-medium blue"> <i class="large material-icons"> mode_edit </i></a>
                                            <a href="{{action('PadraoController@exclui', $pad->id)}}" class="btn-floating btn-medium red"> <i class="large material-icons"> delete </i></a>
                                          </td>
                                        </tr>
                                      @endforeach
                                      </tbody>
                                    </table>@else

                                    <div class="icon-block">
                                      <h2 class="center grey-text"><i class="material-icons">not_interested</i></h2>
                                      <h5 class="center">Nenhum Padrão Cadastrado.</h5>
                                    </div>@endif
                                    <a class="btn-flat btn largura, rigth" href="{{ url('/home') }}">Cancelar</a>

                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- </body> -->

@endsection
