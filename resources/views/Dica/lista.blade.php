@extends('layouts.app')
@section('content')
<!-- <body class="qualquer"> -->
<body background="/img/10.jpg">
  <div class="parallax-container-my">
    <!-- <div class="parallax"><img src="/img/10.jpg"> -->
      <br>
        <div class="container">
            <div class="row">

                <div class="col s12 m8 20 offset-m2 offset-2">
                    <div class="card card-center white">
                        <div class="card-content black-text">
                            <div class="center">
                                <span class="card-title">Dicas para Designer!</span>
                            </div>

                            <div class="col-md-12">@if($dicas != null)
                              <table>
                                      <thead>
                                        <tr>
                                            <th> Dica </th>
                                            <th> Ação </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      @foreach ($dicas as $dica)
                                        <tr>
                                          <td>{{ $dica->nome }}</td>
                                          <td>
                                            <a href="{{action('DicaController@mostra', $dica->id)}}" class="btn-floating btn-medium green"> <i class="large material-icons"> zoom_in </i></a>
                                            <a href="{{action('DicaController@edita', $dica->id)}}" class="btn-floating btn-medium blue"> <i class="large material-icons"> mode_edit </i></a>
                                            <a href="{{action('DicaController@exclui', $dica->id_dica)}}" class="btn-floating btn-medium red"> <i class="large material-icons"> delete </i></a>

                                          </td>
                                        </tr>
                                      @endforeach
                                      </tbody>
                                    </table>@else

                                    <div class="icon-block">
                                      <h2 class="center grey-text"><i class="material-icons">not_interested</i></h2>
                                      <h5 class="center">Nenhuma Dica cadastrada.</h5>
                                    </div>@endif
                                    <a class="btn-flat btn largura" href="{{action('PerfilController@perfil')}}">Cancelar</a>

                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- </body> -->

@endsection
