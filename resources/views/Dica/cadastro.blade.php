@extends('layouts.app')


@section('content')
<!-- <div class="parallax-container-my">
  <div class="parallax"><img src="/img/10.jpg"></div>
<br> -->
<body background="/img/10.jpg">
<div class="container">
<div class="row">
  <div class="col s12 m8 20 offset-m2 offset-2">
  <div class="card card-center white">
          <div class="card-content black-text">
              <!-- <div class="center"> -->
              <div class="center">
                  <span class="card-title">Cadastre a Dica!</span>
              </div>
                  <form action="{{action('DicaController@adiciona')}}"  method="post" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">

               <div class="input-field col s12">
                 <i class="material-icons prefix">mode_edit</i>
                  <input id="name" name="nome" type="text" class="active validate" required>
                  <label for="name">Nome</label>
               </div>

            </div>

            <div class="row">
               <div class="input-field col s12">
			      <i class="material-icons prefix">mode_edit</i>
                  <textarea id="descricao" name="descricao" class="materialize-textarea"></textarea>
                  <label for="descricao">Descrição</label>
               </div>
            </div>
            <div class="card-action">
              <div class="center">
                                <button type="submit" class="btn  grey darken-1 largura">
                    <i class="fa fa-btn fa-sign-in"></i> Cadastrar
                </button>
                  <a class="btn-flat btn largura" href="{{action('PerfilController@perfil')}}">Cancelar</a>
              </div>
            </div>
         </form>
      </div>
</div>
</div>
</div>
</div>
</div>
<!-- </body> -->

@endsection
