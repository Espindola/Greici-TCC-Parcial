@extends('layouts.app')
@section('content')
<!-- <body class="qualquer"> -->
<body background="/img/10.jpg">
  <div class="parallax-container-my">
    <!-- <div class="parallax"><img src="/img/10.jpg"> -->
      <br>
        <div class="container">
            <div class="row">

                <div class="col s12 m8 20 offset-m2 offset-2">
                    <div class="card card-center white">
                        <div class="card-content black-text">


                                    <div class="icon-block">
                                      <h2 class="center grey-text"><i class="material-icons">done</i></h2>
                                      <h5 class="center">Aperação Realizada com Sucesso!.</h5>
                                    </div>
                                    <div class="card-action">
                                      <div class="center">
                                            <a class="btn  grey darken-1 largura "href="{{action('DicaoController@novo')}}">Novo</a>
                                          <a class="btn-flat btn largura" href="{{action('PerfilController@perfil')}}">Ok</a>
                                      </div>
                                    </div>

                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- </body> -->

@endsection
