@extends('layouts.app')
@section('content')
<body background="/img/10.jpg">
  <div class="parallax-container-my">
    <!-- <div class="parallax"><img src="/img/10.jpg"> -->
      <br>
        <div class="container">
            <div class="row">

                <div class="col s12 m8 20 offset-m2 offset-2">
                    <div class="card card-center white">
                        <div class="card-content black-text">@if($dica != null)
                            <div class="center">
                                <span class="card-title">Dica: {{ $dica->nome }}</span>
                            </div>
                            <div class="center">
                                <span class="card-title">Descrição: {{ $dica->descricao }}</span>
                            </div>

                            <div class="card-action">
                              <div class="center">
                                    <a  href= "{{ action('DicaController@lista') }}" type="submit" class="btn grey darken-1 largura"> OK </a>
                                </button>
                                  <a class="btn-flat btn largura" href= "{{ action('DicaController@lista') }}">Cancelar</a>
                              </div>
                            </div>
                            @else

                                    <div class="icon-block">
                                      <h2 class="center grey-text"><i class="material-icons">not_interested</i></h2>
                                      <h5 class="center">Dica não encontrada.</h5>
                                    </div>@endif

                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- </body> -->

@endsection
