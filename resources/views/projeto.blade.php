@extends('layouts.app')

@section('content')

<main role="main">
</main>

<section class="portfolio">
  <section class="portfolio-heading">
    <div class="container text-center">
      <h2>Portfolio</h2>
      <p>Vestibulum laoreet euismod sapien, at tristique ante pretium nec.</p>
    </div>
  </section>
  <div class="container">
    <div class="text-center">
      <div class="btn-group filter" role="group" aria-label="">
        <button type="button" class="btn btn-default active" data-filter="*">All</button>
        <button type="button" class="btn btn-default" data-filter=".code">Coding</button>
        <button type="button" class="btn btn-default" data-filter=".design">Design</button>
      </div>
    </div>
    <div class="grid">
      <div class="grid-item code">
        <figure class="effect">
          <a href="#"><img src="img/3.jpg" alt="" class="img-responsive"></a>
          <figcaption>
            <div class="caption">
              <h5 class="text-uppercase">Portfolio Item</h5>
              <span class="text-muted">Code</span>
            </div>
          </figcaption>
        </figure>
      </div>
      <div class="grid-item design">
        <figure class="effect">
          <a href="#"><img src="img/1.jpg" alt="" class="img-responsive"></a>
          <figcaption>
            <div class="caption">
              <h5 class="text-uppercase">Portfolio Item</h5>
              <span class="text-muted">Design</span>
            </div>
          </figcaption>
        </figure>
      </div>
      <div class="grid-item code">
        <figure class="effect">
          <a href="#"><img src="img/4.jpg" alt="" class="img-responsive"></a>
          <figcaption>
            <div class="caption">
              <h5 class="text-uppercase">Portfolio Item</h5>
              <span class="text-muted">Code</span>
            </div>
          </figcaption>
        </figure>
      </div>
      <div class="grid-item hidden-xs hidden-sm design">
        <figure class="effect">
          <a href="#"><img src="img/2.jpg" alt="" class="img-responsive"></a>
          <figcaption>
            <div class="caption">
              <h5 class="text-uppercase">Portfolio Item</h5>
              <span class="text-muted">Design</span>
            </div>
          </figcaption>
        </figure>
      </div>
      <div class="grid-item hidden-xs hidden-sm code">
        <figure class="effect">
          <a href="#"><img src="img/6.jpg" alt="" class="img-responsive"></a>
          <figcaption>
            <div class="caption">
              <h5 class="text-uppercase">Portfolio Item</h5>
              <span class="text-muted">Code</span>
            </div>
          </figcaption>
        </figure>
      </div>
      <div class="grid-item hidden-xs hidden-sm design">
        <figure class="effect">
          <a href="#"><img src="img/5.jpg" alt="" class="img-responsive"></a>
          <figcaption>
            <div class="caption">
              <h5 class="text-uppercase">Portfolio Item</h5>
              <span class="text-muted">Design</span>
            </div>
          </figcaption>
        </figure>
      </div>
    </div>
  </div>
  <section class="portfolio-btn">
    <div class="container text-center">
      <button type="button" class="btn btn-default">View Portfolio</button>
    </div>
  </section>
</section>
<section class="featurete">
  <div class="container">
    <div class="row">
      <div class="col-md-4 text-right">
        <div class="micro-feature">
          <h4 class="text-uppercase">Feature</h4>
          <p>Phasellus in auctor magna. Suspendisse vel maximus nibh, auctor magna. Suspendisse vel maximus nibh.</p>
        </div>
        <hr>
        <div class="micro-feature">
          <h4 class="text-uppercase">Feature</h4>
          <p>Phasellus in auctor magna, vestibulum laoreet euismod sapien, at tristique ante pretium nec. Phasellus in auctor magna. Suspendisse vel maximus nibh.</p>
        </div>
        <hr>
        <div class="micro-feature">
          <h4 class="text-uppercase">Feature</h4>
          <p>Phasellus in auctor magna, vestibulum laoreet euismod sapien, at tristique ante pretium nec. Phasellus in auctor magna. Suspendisse vel maximus nibh.</p>
        </div>
      </div>
      <div class="col-md-4">
        <figure><img src="img/iphone.png" alt="" class="img-responsive"</figure>
      </div>
      <div class="col-md-4 hidden-xs hidden-sm text-left">
        <div class="micro-feature">
          <h4 class="text-uppercase">Feature</h4>
          <p>Phasellus in auctor magna, vestibulum laoreet euismod sapien, at tristique ante pretium nec. Phasellus in auctor magna. Suspendisse vel maximus nibh.</p>
        </div>
        <hr>
        <div class="micro-feature">
          <h4 class="text-uppercase">Feature</h4>
          <p>Phasellus in auctor magna, vestibulum laoreet euismod sapien, at tristique ante pretium nec. Phasellus in auctor magna. Suspendisse vel maximus nibh.</p>
        </div>
        <hr>
        <div class="micro-feature">
          <h4 class="text-uppercase">Feature</h4>
          <p>Phasellus in auctor magna, vestibulum laoreet euismod sapien, at tristique ante pretium nec. Phasellus in auctor magna. Suspendisse vel maximus nibh.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="mini-banner mini-featurete">
  <div class="container text-center">
    <p>Ut sed volutpat nisi. Nunc eu metus interdum ligula viverra rutrum.</p>
    <a href="#"><button type="button" class="btn btn-default">Buy Now</button></a>
  </div>
</section>

@endsection
