@extends('layouts.app')

@section('content')

<body class="qualquer">
<div class="parallax-container-my">
  <div class="parallax"><img src="/img/10.jpg"></div>
<br>
<div class="container">
    <div class="row">

        <div class="col s12 m8 l6 offset-m2 offset-l3">
            <div class="card card-center white">
                <div class="card-content black-text">
                    <div class="center">
                        <span class="card-title">Login</span>
                    </div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Senha</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                          <p>
                             <input type="checkbox" id="lembrar" />
                             <label for="lembrar">Lembre-me</label>
                           </p>

                            <div class="card-action">
                                <button type="submit" class="btn  grey darken-1 largura">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>
                                  <!-- <a class="btn-flat btn largura" href="{{ url('/home') }}">Cancelar</a> -->
                                <a class="btn-flat btn largur" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                              </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- </body> -->

@endsection
