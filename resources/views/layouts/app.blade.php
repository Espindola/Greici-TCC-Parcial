<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Desart</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/init.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
        $('select').material_select();
      });
  </script>
  <script type="text/javascript">
      $(document).ready(function() {
    $('input#input_text, textarea#textarea1').characterCounter();
  });
  </script>
</head>
<body>
  <nav class="white" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="{{ url('/') }}" class="brand-logo">Desart</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">Home</a></li>
        <li><a href="#">Sobre</a></li>
        <li><a href="#">Contato</a></li>
        <li><a href="#">Ajuda</a></li>
				@if (Auth::guest())
					<li><a href="{{url('login')}}" class="waves-effect waves-light btn  grey">Login</button></a></li>
					<li><a href="{{url('register')}}"  class="waves-effect waves-light btn grey">Registrar</button></a></li>
				@else
              <li><a href="{{action('PerfilController@perfil')}}">Perfil</a></li>
							<li><a href="{{ url('/logout') }}" class="waves-effect waves-light btn grey"> Sair </button></a></li>
				 @endif
      </ul>

      <ul id="nav-mobile" class="side-nav">
        <li><a href="{{ url('/home') }}">Home</a></li>
        <li><a href="#">Sobre</a></li>
        <li><a href="#">Contato</a></li>
        <li><a href="#">Ajuda</a></li>
				@if (Auth::guest())
					<li><a href="{{url('login')}}" class="waves-effect waves-light btn grey">Login</button></a></li>
					<li><a href="{{url('register')}}"  class="waves-effect waves-light btn grey">Registrar</button></a></li>
				@else
							<li><a href="{{ url('/logout') }}" class="waves-effect waves-light btn grey">Sair</button></a></li>
				 @endif
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
  </nav>

    @yield('content')

		<!-- <footer class="page-footer teal">
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="brown-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer> -->


  <!--  Scripts
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script-->

  </body>
</html>
