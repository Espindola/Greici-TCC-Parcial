@extends('layouts.app')
@section('content')
  <!-- <body class="qualquer"> -->
    <div class="parallax-container-my">
      <!-- <div class="parallax"><img src="/img/10.jpg"> -->
        <br>
          <div class="container">
              <div class="row">

                  <div class="col s12 m8 20 offset-m2 offset-2">
                      <div class="card card-center white">
                          <div class="card-content black-text">

                        <div class="icon-block">
                            <h2 class="center grey-text"><i class="material-icons">not_interested</i></h2>
                                <h5 class="center">Página não encontrada!.</h5>
                                <p class="center">Endereço informado está incorreto.</p>
                          </div>

                          </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  </div>
  </div>
  <!-- </body> -->

  @endsection
