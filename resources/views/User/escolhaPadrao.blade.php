@extends('layouts.app')
@section('content')

<body class="qualquer">
  <div class="parallax-container-my">
    <div class="parallax"><img src="/img/10.jpg"></div>
      <br>
        <div class="container">
            <div class="row">

                <div class="col s12 m8 l6 offset-m2 offset-l3">
                    <div class="card card-center white">
                      <div class="card-content black-text">
                          <div class="center">
                              <span class="card-title">Escolha Seu Projeto!</span>
                          </div>
                          <form class="form-horizontal" role="form" method="GET" action="{{ url('/login') }}">
                            {{ csrf_field() }}

                        <!-- <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nome</label> -->
<div class="input-field">
                            <select>
                                <option value=""></option>
                                    <option value="1"> Cartao de Visita</option>
                                    <option value="2"> Poster</option>
                            </select>
                            <label>Escolha o Padrão</label>
                          </div>

                        <div class="card-action">
                            <button type="submit" class="btn  grey darken-1 largura">
                                <i class="fa fa-btn fa-sign-in"></i>Criar
                            </button>
                              <!-- <a class="btn-flat btn largura" href="{{ url('/home') }}">Cancelar</a> -->
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
