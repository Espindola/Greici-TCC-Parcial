<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Desart</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/init.js"></script>
<!--
  <script type="text/javascript">
      $(document).ready(function() {
        $('select').material_select();
      });
  </script>
  <script type="text/javascript">
      $(document).ready(function() {
    $('input#input_text, textarea#textarea1').characterCounter();
  });
  </script>
</head> -->
<body background="/img/10.jpg">
<nav><ul id="slide-out" class="side-nav fixed">

  <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header">Greici<i class="material-icons">person arrow_drop_down</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="#!">Sair</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>
<p></p>

  <!-- <li><a  href="{{ url('/') }}"><i class="material-icons">cloud</i>Home</a></li> -->
  <li>
    <div class="divider"></div>
  </li>

  <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header">Gerenciar Padrões<i class="material-icons">mode_edit arrow_drop_down</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="{{action('PadraoController@novo')}}">Novo</a></li>
                <li><a href="{{action('PadraoController@lista')}}">Listar</a></li>
                <li><a href="{{action('PadraoController@lista')}}">Excluir</a></li>
                <li><a href="{{action('PadraoController@lista')}}">Editar</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>

      <p><p>

      <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
              <li>
                <a class="collapsible-header">Gerenciar Leis<i class="material-icons">mode_edit arrow_drop_down</i></a>
                <div class="collapsible-body">
                  <ul>
                    <li><a href="{{action('LeiController@novo')}}">Novo</a></li>
                    <li><a href="{{action('LeiController@lista')}}">Listar</a></li>
                    <li><a href="{{action('LeiController@lista')}}">Excluir</a></li>
                    <li><a href="{{action('LeiController@lista')}}">Editar</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </li>
          <p><p>
          <li class="no-padding">
                <ul class="collapsible collapsible-accordion">
                  <li>
                    <a class="collapsible-header">Gerenciar Dicas<i class="material-icons">mode_edit arrow_drop_down</i></a>
                    <div class="collapsible-body">
                      <ul>
                        <li><a href="{{action('DicaController@new')}}">Novo</a></li>
                        <li><a href="{{action('DicaController@lista')}}">Listar</a></li>
                        <li><a href="{{action('DicaController@lista')}}">Excluir</a></li>
                        <li><a href="{{action('DicaController@lista')}}">Editar</a></li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </li>
              <p><p>
  <li><a class="waves-effect" href="{{action('DesenhoController@new')}}">Desenho Publicitario <i class="material-icons">mode_edit</i></a></li>
  <li><a class="waves-effect" href="{{action('DesenhoController@criarL')}}">Desenho Livre<i class="material-icons">mode_edit</i></a></li>

</ul>
</nav>
    <!-- <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a> -->

  <!-- Navbar goes here -->
      <!-- Page Layout here -->
<div class="row">
  <div class="col s3">

</div>

        </div>
