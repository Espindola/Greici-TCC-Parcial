<script type="text/javascript" src="paperjs/dist/paper-full.js"></script>

<script type="text/javascript" >
     paper.install(window);
     // Keep global references to both tools, so the HTML
     // links below can access them.
     var toolDesenho, toolNuvens, tool3, tool4,toolLinhasG, toolCirc, toolFig1,
      toolLinhaTrans, toolCirculos, toolLadoalado;

     window.onload = function() {
       paper.setup('myCanvas');

       // Create two drawing tools.
       // tool1 will draw straight lines,
       // tool2 will draw clouds.
       var path;
       // Both share the mouseDown event:
       //  path.strokeColor = 'black';
       function onMouseDown(event) {
         path = new Path();
         path.strokeColor = 'black';
         path.add(event.point);
         return path;
       }

     ///CRIA LINAS
       toolDesenho = new Tool();
       toolDesenho.onMouseDown = onMouseDown;

       toolDesenho.onMouseDrag = function(event) {
         path.add(event.point);
       }

       toolDesenho.onMouseUp = function(event){
           path.simplify(10);
       }

     //CRIA NUVENS
       toolNuvens = new Tool();
       toolNuvens.minDistance = 20;
       toolNuvens.onMouseDown = onMouseDown;

       toolNuvens.onMouseDrag = function(event) {
         // Use the arcTo command to draw cloudy lines
         path.arcTo(event.point);
       }

     //CRIA POR PONTOS INCOMPLETO
       tool3 = new Tool();
       tool3.onMouseDown = onMouseDown;

       tool3.onMouseDown = function(event) {
       // Add a segment to the path at the position of the mouse:
       path.add(event.point);
       }

     // 		// Create a new path once, when the script is executed:
     // var myPath = new Path();
     // myPath.strokeColor = 'black';
     //
     // // This function is called whenever the user
     // // clicks the mouse in the view:
     // function onMouseDown(event) {
     // 	// Add a segment to the path at the position of the mouse:
     // 	myPath.add(event.point);
     // }




     //VARIS CIRCULOS
     toolCirculos =  new Tool();
     toolCirculos.minDistance = 20;

     toolCirculos.onMouseDrag = function(event) {
     	var circle = new Path.Circle({
     		center: event.middlePoint,
     		radius: event.delta.length / 2
     	});
     	circle.fillColor = 'black';
     }

     ///MENOR
     // tool.maxDistance = 10;
     //
     // function onMouseDrag(event) {
     // 	var circle = new Path.Circle({
     // 		center: event.middlePoint,
     // 		radius: event.delta.length / 2
     // 	});
     // 	circle.fillColor = 'black';
     // }

     ///MESMO TAMANHO
     // project.currentStyle.fillColor = 'black';
     //
     // tool.fixedDistance = 30;
     //
     // function onMouseDrag(event) {
     // 	var circle = new Path.Circle({
     // 		center: event.middlePoint,
     // 		radius: event.delta.length / 2
     // 	});
     // 	circle.fillColor = 'black';
     // }


     //LINHAS UMA DO LADO DA onDocumentDrag

     // The mouse has to be moved at least 10 pt
     // // before the next onMouseDrag event is called:
     toolLadoalado =  new Tool();
     toolLadoalado.minDistance = 10;

     toolLadoalado.onMouseDrag = function(event) {
     	var path = new Path();
     	path.strokeColor = 'black';
     	var vector = event.delta;

     	// rotate the vector by 90 degrees:
     	vector.angle += 90;

     	// change its length to 5 pt:
     	vector.length = 5;

     	path.add(event.middlePoint + vector);
     	path.add(event.middlePoint - vector);
     }


     //LINHAS UMA DO LADO DA OUTRA COM UMA NO MEIO

     toolLinhaTrans = new Tool();
     toolLinhaTrans.minDistance = 10;

     var path;

     toolLinhaTrans.onMouseDown = function(event) {
     	path = new Path();
     	path.strokeColor = '#00000';

     	path.add(event.point);
     }

     toolLinhaTrans.onMouseDrag = function(event) {
     	path.add(event.point);

     	var step = event.delta;
     	step.angle += 90;

     	var top = event.middlePoint + step;
     	var bottom = event.middlePoint - step;

     	var line = new Path();
     	line.strokeColor = '#000000';
     	line.add(top);
     	line.add(bottom);
     }

     //LINHAS GROSSAS
     toolLinhasG =  new Tool();
     toolLinhasG.minDistance = 10;
     toolLinhasG.maxDistance = 45;

     toolLinhasG.onMouseDown = function(event) {
     path = new Path();
     path.fillColor = 'black';
     // {
     // 	hue: Math.random() * 360,
     // 	saturation: 1,
     // 	brightness: 1
     // };
     path.add(event.point);
     }

     toolLinhasG.onMouseDrag =function(event) {
     var step = event.delta / 2;
     step.angle += 90;

     var top = event.middlePoint + step;
     var bottom = event.middlePoint - step;

     path.add(top);
     path.insert(0, bottom);
     path.smooth();
     }

     toolLinhasG.onMouseUp = function(event) {
     path.add(event.point);
     path.closed = true;
     path.smooth();
     }

     //DESENHO ENGRAÇADO (PARECE UM PENTE)
     toolFig1 =  new Tool();
     toolFig1.fixedDistance = 30;

     var strokeEnds = 6;

     toolFig1.onMouseDown = function(event) {
     	path = new Path();
     	path.fillColor = 'black;'
       // {
     	// 	hue: Math.random() * 360,
     	// 	saturation: 1,
     	// 	brightness: 1
     	// };
     }

     var lastPoint;
     toolFig1.onMouseDrag = function(event) {
     	// If this is the first drag event,
     	// add the strokes at the start:
     	if(event.count == 0) {
     		toolFig1.addStrokes(event.middlePoint, event.delta * -1);
     	} else {
     		var step = event.delta / 2;
     		step.angle += 90;

     		var top = event.middlePoint + step;
     		var bottom = event.middlePoint - step;

     		path.add(top);
     		path.insert(0, bottom);
     	}
     	path.smooth();

     	lastPoint = event.middlePoint;
     }

     toolFig1.onMouseUp = function(event) {
     	var delta = event.point - lastPoint;
     	delta.length = tool.maxDistance;
     	toolFig1.addStrokes(event.point, delta);
     	path.closed = true;
     	path.smooth();
     }

     toolFig1.addStrokes = function(point, delta) {
     	var step = delta.rotate(90);
     	var strokePoints = strokeEnds * 2 + 1;
     	point -= step / 2;
     	step /= strokePoints - 1;
     	for(var i = 0; i < strokePoints; i++) {
     		var strokePoint = point + step * i;
     		var offset = delta * (Math.random() * 0.3 + 0.1);
     		if(i % 2) {
     			offset *= -1;
     		}
     		strokePoint += offset;
     		path.insert(0, strokePoint);
     	}
     }


     //EFEITO QBERTY
     // var values = {
     // 		amount: 30
     // };
     //
     // var raster, group;
     // var piece = createPiece();
     // var count = 0;
     //
     // handleImage('mona');
     //
     // var text = new PointText({
     // 		point: view.center,
     // 		justification: 'center',
     // 		fillColor: 'white',
     // 		fontSize: 15,
     // 		content: window.FileReader
     // 				? 'Drag & drop an image from your desktop'
     // 				: 'To drag & drop images, please use Webkit, Firefox, Chrome or IE 10'
     // });
     //
     // function createPiece() {
     // 		var group = new Group();
     // 		var hexagon = new Path.RegularPolygon({
     // 				center: view.center,
     // 				sides: 6,
     // 				radius: 50,
     // 				fillColor: 'gray',
     // 				parent: group
     // 		});
     // 		for (var i = 0; i < 2; i++) {
     // 				var path = new Path({
     // 						closed: true,
     // 						selected: true,
     // 						parent: group,
     // 						fillColor: i == 0 ? 'white' : 'black'
     // 				});
     // 				for (var j = 0; j < 3; j++) {
     // 						var index = (i * 2 + j) % hexagon.segments.length;
     // 						path.add(hexagon.segments[index].clone());
     // 				}
     // 				path.add(hexagon.bounds.center);
     // 		}
     // 		// Remove the group from the document, so it is not drawn:
     // 		group.remove();
     // 		return group;
     // }
     //
     // function handleImage(image) {
     // 		count = 0;
     // 		var size = piece.bounds.size;
     //
     // 		if (group)
     // 				group.remove();
     //
     // 		raster = new Raster(image);
     // 		raster.remove();
     //
     // 		// Transform the raster, so it fills the view:
     // 		raster.fitBounds(view.bounds, true);
     // 		group = new Group();
     // 		for (var y = 0; y < values.amount; y++) {
     // 				for (var x = 0; x < values.amount; x++) {
     // 						var copy = piece.clone();
     // 						copy.position += size * [x + (y % 2 ? 0.5 : 0), y * 0.75];
     // 						group.addChild(copy);
     // 				}
     // 		}
     //
     // 		// Transform the group so it covers the view:
     // 		group.fitBounds(view.bounds, true);
     // 		group.scale(1.1);
     // }
     //
     // function onFrame(event) {
     // 		// Loop through the uncolored hexagons in the group and fill them
     // 		// with the average color:
     // 		var length = Math.min(count + values.amount, group.children.length);
     // 		for (var i = count; i < length; i++) {
     // 				piece = group.children[i];
     // 				var hexagon = piece.children[0];
     // 				var color = raster.getAverageColor(hexagon);
     // 				if (color) {
     // 						hexagon.fillColor = color;
     // 						// hexagon.strokeColor = color;
     // 						var top = piece.children[1];
     // 						top.fillColor = color.clone();
     // 						top.fillColor.brightness *= 1.5;
     //
     // 						var right = piece.children[2];
     // 						right.fillColor = color.clone();
     // 						right.fillColor.brightness *= 0.5;
     // 				}
     // 		}
     // 		count += values.amount;
     // }
     //
     // function onResize() {
     // 		project.activeLayer.position = view.center;
     // }
     //
     // function onDocumentDrag(event) {
     // 		event.preventDefault();
     // }
     //
     // function onDocumentDrop(event) {
     // 		event.preventDefault();
     //
     // 		var file = event.dataTransfer.files[0];
     // 		var reader = new FileReader();
     //
     // 		reader.onload = function ( event ) {
     // 				var image = document.createElement('img');
     // 				image.onload = function () {
     // 						handleImage(image);
     // 						view.update();
     // 				};
     // 				image.src = event.target.result;
     // 		};
     // 		reader.readAsDataURL(file);
     // }
     //
     // document.addEventListener('drop', onDocumentDrop, false);
     // document.addEventListener('dragover', onDocumentDrag, false);
     // document.addEventListener('dragleave', onDocumentDrag, false);

     //SELECIONA
     var hitOptions = {
       segments: true,
       stroke: true,
       fill: true,
       //tolerance: 5
     };

     var segment;
     var movePath = false;

     tool4 = new Tool();
     tool4.onMouseDown = onMouseDown;
     tool4.onMouseDown = function(event) {
       //path.smooth();
      //	path.fullySelected = true; //Seleciona
       segment = path = null;
       var hitResult = project.hitTest(event.point, hitOptions);
       if (!hitResult)
           return;

       if (event.modifiers.shift) {
           if (hitResult.type == 'segment') {
               hitResult.segment.remove();
           };
           return;
       }

       if (hitResult) {
           path = hitResult.item;
           if (hitResult.type == 'segment') {
               segment = hitResult.segment;
           } else if (hitResult.type == 'stroke') {
               var location = hitResult.location;
               segment = path.insert(location.index + 1, event.point);
               path.smooth();
           }
       }
       movePath = hitResult.type == 'fill';
       if (movePath)
           project.activeLayer.addChild(hitResult.item);
     }

     tool4.onMouseMove = function(event) {
       project.activeLayer.selected = false;
       if (event.item)
           event.item.selected = true
     }

     tool4.onMouseDrag = function(event) {
       console.log(segment.point)
       console.log(event.delta)

       if (segment) {
           segment.point += event.delta;
           path.smooth();
       } else if (path) {
           path.position += event.delta;
       }
     }
     ////////////
       toolCirc = new Tool();
       //toolCirc.onMouseUp = onMouseUp;
       toolCirc.onMouseUp = function(event) {
         var circle = new Path.Circle({
           center: event.middlePoint,
           radius: event.delta.length / 2
         });
         circle.strokeColor = 'black';
         circle.fillColor = 'black';
       }

      //  drawColorPalette("mydiv", function(color) {
      //      document.getElementById("textcolor").innerHTML = color;
      //  });

     }

     // function Salvar(){
     // 	alert(project);
     // 	project.exportSVG();
     // }

     </script>
