@extends('layouts.app')


@section('content')
<!-- <div class="parallax-container-my">
  <div class="parallax"><img src="/img/10.jpg"></div>
<br> -->
<body background="/img/10.jpg">
<div class="container">
<div class="row">
  <div class="col s12 m8 20 offset-m2 offset-2">
  <div class="card card-center white">
          <div class="card-content black-text">
              <!-- <div class="center"> -->

              <div class="center">
                  <span class="card-title">Escolhe Padrão!</span>
              </div>
              @if($pads != null)
                  <form action="{{action('DesenhoController@criar')}}"  method="get" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="row">
                        <div class="input-field col s12">
                        <select name="padroes">
                          <option value="" disabled selected>Esolha uma opção</option>
                          @foreach ($pads as $pad)
                          <option value="{{$pad->id_padrao }}">{{$pad->nome }}</option>
                            @endforeach
                        </select>
                        <label>Padrões</label>
                      </div>
                    </div>

            <div class="card-action">
              <div class="center">
                                <button type="submit" class="btn  grey darken-1 largura">
                    <i class="fa fa-btn fa-sign-in"></i> Criar
                </button>
                  <a class="btn-flat btn largura" href="{{action('PerfilController@perfil')}}">Cancelar</a>
              </div>
            </div>
         </form>
         @else

         <div class="icon-block">
           <h2 class="center grey-text"><i class="material-icons">not_interested</i></h2>
           <h5 class="center">Nenhuma Padrão Cadastrado.</h5>
         </div>@endif
      </div>
</div>
</div>
</div>
</div>
</div>
<!-- </body> -->

@endsection
