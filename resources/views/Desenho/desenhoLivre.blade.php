@extends('layouts.app')

@section('content')
<script type="text/javascript" >
$(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
  });
</script>
<script  src="/js/jscolor.js"></script>
<script  src="/paperjs/dist/paper-core.js"></script>
<script  src="/paperjs/dist/paper-full.js"></script>

<script type="text/javascript">
window.onbeforeunload = confirmar_saida;

function confirmar_saida()
{
    return ' Se você fechar essa janela irá perder tudo seu trabalho até agora!';
}
</script>
<script type"text/javascript">

var tamFonte, formFonte, fontEscrita, texto;
var fontesUsadas = ["nova fonte", "nova fonte", "nova fonte"];

/////////////////////////////////////////////////////
     paper.install(window);
     // Keep global references to both tools, so the HTML
     // links below can access them.
     var toolCor,toolEscreve, toolDesenho, toolNuvens,  toolSeleciona, toolCirc, toolPequeno, toolCirculosMedio, toolCirculosGrande, cont;
    //  var tamFonte, formFonte, fontEscrita, texto, cont;
    //  var fontesUsadas = ["nova fonte", "nova fonte", "nova fonte"];
     cor = 'black';

     window.onload = function() {

       paper.setup('myCanvas');
//
// toolCor = new Tool();
//        toolCor.onMouseDown = function(jscolor) {
//          // 'jscolor' instance can be used as a string
//          var cor = "'" + '#' + jscolor+"''";
//        }
//

toolEscreve = new Tool();
toolEscreve.onMouseDown = function(event){
  // tamFonte = document.getElementById("tam").value;
  // // form.tam.value;
	// formFonte = document.getElementById("formatacao").value;
  // // form.formatacao.value;
	// fonteEscrita = document.getElementById("fonte").value;
  // // form.fonte.value;
	// texto = document.getElementById("texto").value;
		// if(cont==0){
		// 	cont++;
		// 	fontesUsadas[0] = fonteEscrita;
      var text = new PointText(new Point(event.point, event.point));
      text.fillColor = '#'+document.getElementById("cor").value;
      text.fontFamily = document.getElementById("fonte").value;
      text.fontSize = document.getElementById("tam").value;

  // Set the content of the text item:
  text.content = document.getElementById("texto").value;
  return text
		// }else
		// 	if(cont==3){
		// 		var aux = 0;
		// 		for (i = 0; i < fontesUsadas.length-1; i++) {
		// 	    if(fontesUsadas[i] != fonteEscrita){
		// 				aux++;
		// 			}
		// 		}
		// 		if(aux==3){
		// 				//proibir escrever
		// 		}else{
		// 			var text = new PointText({
		// 		    point: [event.point, event.point],
		// 		    content: texto,
		// 		    fillColor: cor,
		// 		    fontFamily: fonteEscrita,
		// 		    fontWeight: formFonte,
		// 		    fontSize: tamFonte
		// 			});
		// 		  return text
		// 		}
		// }else
		// 	if(cont==1){
		// 		if(fontesUsadas[0] == fonteEscrita){
		// 			cont++;
		// 			fontesUsadas[1] = fonteEscrita;
		// 		}
		// 		var text = new PointText({
		// 	    point: [event.point, event.point],
		// 	    content: texto,
		// 	    fillColor: cor,
		// 	    fontFamily: fonteEscrita,
		// 	    fontWeight: formFonte,
		// 	    fontSize: tamFonte
		// 	});
		// 	  return text
		// 	}else {
		// 		if(cont==2){
		// 			var aux=0;
		// 			for (i = 0; i < fontesUsadas.length-2; i++) {
		// 				if(fontesUsadas[i] !=fonteEscrita){
		// 					aux++;
		// 				}
		// 				if(aux!=0){
		// 					cont++;
		// 					fontesUsadas[2] = fonteEscrita;
		// 				}
		// 			}
		// 			var text = new PointText({
		// 		    point: [event.point, event.point],
		// 		    content: texto,
		// 		    fillColor: cor,
		// 		    fontFamily: fonteEscrita,
		// 		    fontWeight: formFonte,
		// 		    fontSize: tamFonte
		// 		});
		// 		  return text
		// 		}
			// }
}



       // Create two drawing tools.
       // tool1 will draw straight lines,
       // tool2 will draw clouds.
       var path;
       // Both share the mouseDown event:
       //  path.strokeColor = 'black';
       function onMouseDown(event) {
         path = new Path();
         path.strokeColor = '#'+document.getElementById("cor").value;
         path.add(event.point);
         return path;
       }

     ///CRIA LINhAS
       toolDesenho = new Tool();
       toolDesenho.onMouseDown = onMouseDown;

       toolDesenho.onMouseDrag = function(event) {
         path.add(event.point);
       }

       toolDesenho.onMouseUp = function(event){
           path.simplify(10);
       }

     //CRIA NUVENS
       toolNuvens = new Tool();
       toolNuvens.minDistance = 20;
       toolNuvens.onMouseDown = onMouseDown;

       toolNuvens.onMouseDrag = function(event) {
         // Use the arcTo command to draw cloudy lines
         path.arcTo(event.point);
       }

     ///Escreve

    //  toolEscreve = new Tool();
    //  toolEscreve.onMouseDown = function(event) {
    //    var text = new PointText(new Point(event.point, event.point));
    //    text.fillColor = 'black';
     //
    //    // Set the content of the text item:
    //    text.content = 'Hello world';
    //    return text
    //  }


     //MENOR
     toolPequeno = new Tool();
     toolPequeno.maxDistance = 5;

     toolPequeno.onMouseDrag =function(event) {
       var circle = new Path.Circle({
         center: event.middlePoint,
         radius: event.delta.length / 2
       });
       circle.fillColor = cor;
     }

     ///MESMO Grande
     toolCirculosMedio = new Tool();
     toolCirculosMedio.fixedDistance = 25;

     toolCirculosMedio.onMouseDrag = function(event) {
     	var circle = new Path.Circle({
     		center: event.middlePoint,
     		radius: event.delta.length / 2
     	});
     	circle.fillColor = '#'+document.getElementById("cor").value;
     }

     ///MESMO Grande
     toolCirculosGrande = new Tool();
     toolCirculosGrande.fixedDistance = 50;

     toolCirculosGrande.onMouseDrag = function(event) {
       var circle = new Path.Circle({
         center: event.middlePoint,
         radius: event.delta.length / 2
       });
       circle.fillColor = '#'+document.getElementById("cor").value;
     }



     //SELECIONA
     var hitOptions = {
       segments: true,
       stroke: true,
       fill: true,
       //tolerance: 5
     };

     var segment;
     var movePath = false;

     toolSeleciona= new Tool();
     toolSeleciona.onMouseDown = onMouseDown;
     toolSeleciona.onMouseDown = function(event) {
       //path.smooth();
      //	path.fullySelected = true; //Seleciona
       segment = path = null;
       var hitResult = project.hitTest(event.point, hitOptions);
       if (!hitResult)
           return;

       if (event.modifiers.shift) {
           if (hitResult.type == 'segment') {
               hitResult.segment.remove();
           };
           return;
       }

       if (hitResult) {
           path = hitResult.item;
           if (hitResult.type == 'segment') {
               segment = hitResult.segment;
           } else if (hitResult.type == 'stroke') {
               var location = hitResult.location;
               segment = path.insert(location.index + 1, event.point);
               path.smooth();
           }
       }
       movePath = hitResult.type == 'fill';
       if (movePath)
           project.activeLayer.addChild(hitResult.item);
     }

     toolSeleciona.onMouseMove = function(event) {
       project.activeLayer.selected = false;
       if (event.item)
           event.item.selected = true
     }

     toolSeleciona.onMouseDrag = function(event) {
       console.log(segment.point)
       console.log(event.delta)

       if (segment) {
           segment.point += event.delta;
           path.smooth();
       } else if (path) {
           path.position += event.delta;
       }
     }


     //Circulo
       toolCirc = new Tool();
       //toolCirc.onMouseUp = onMouseUp;
       toolCirc.onMouseUp = function(event) {
         var circle = new Path.Circle({
           center: event.middlePoint,
           radius: event.delta.length / 2
         });
         circle.strokeColor = '#'+document.getElementById("cor").value;
         circle.fillColor = '#'+document.getElementById("cor").value;
       }


     }

//SALVA
function Salvar(fileName){

     	   if(!fileName) {
     	       fileName = "paperjs_example.svg"
     	   }

     	   var url = "data:image/svg+xml;utf8," + encodeURIComponent(paper.project.exportSVG({asString:true}));

     	   var link = document.createElement("a");
     	   link.download = fileName;
     	   link.href = url;
     	   link.click();
     	}

     </script>




</script>
     <!-- Navbar goes here -->
         <!-- Page Layout here -->
<body background="/img/10.jpg">
<div class="row">
     <div class="col s12 m4 l2">
       <br>
       <!-- <div class="card card-white"> -->
            <a class="waves-effect waves-light btn" href="#modal1">Dicas</a>
               <!-- Modal Structure -->
               <div id="modal1" class="modal">
                 @if($dicas != null)
                 <h4>Dicas</h4>
                    <div class="modal-content">
                      @foreach ($dicas as $dica)
                          <p><b>{{ $dica->nome }}</b> :{{ $dica->descricao }}</p>
                      @endforeach
                    </div>
                 <div class="modal-footer">
                   <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
                 </div>
                 @endif
               </div> <br> <br>



      <div class="white-text">

          <div class="row">
             <a href="#" onclick="toolSeleciona.activate();" class="btn-floating btn-medium red, btn tooltipped" data-position="right" data-delay="50" data-tooltip="Seta"><i class="large material-icons"> call_made </i></a><br>
             <a href="#" onclick="toolDesenho.activate();" class="btn-floating btn-medium grey, btn tooltipped" data-position="right" data-delay="50" data-tooltip="Desenha"><i class="large material-icons"> mode_edit </i></a><br>
             <a href="#" onclick="Salvar();" class="btn-floating btn-medium red, btn tooltipped" data-position="right" data-delay="50" data-tooltip="Salvar"><i class="large material-icons"> save </i></a><br>
             <a href="#" onclick="toolNuvens.activate();" class="btn-floating btn-medium red, btn tooltipped" data-position="right" data-delay="50" data-tooltip="Desenha Nuvens"> <i class="large material-icons"> cloud</i></a><br>
             <a href="#" onclick="toolCirc.activate();" class="btn-floating btn-medium red, btn tooltipped" data-position="right" data-delay="50" data-tooltip="Circulo"> <i class="large material-icons"></i></a><br>
             <a href="#" onclick="toolCirculosMedio.activate();"class="btn-floating btn-medium red, btn tooltipped" data-position="right" data-delay="50" data-tooltip="Circulos Juntos">ooooo</a><br>
          </div>

          <!--Parte para escrita-->
           <a href="#" onclick="toolEscreve.activate();"class="btn-floating btn-medium grey, btn tooltipped" data-position="right" data-delay="50" data-tooltip="Escrita">
             <i class="large material-icons"> spellcheck </i>
          </a><br>
          <!-- , , btn tooltipped" data-position="left" data-delay="50" data-tooltip="Clique na tela no lugar desejado para adicionar o texto" -->
          <!--<a class="btn-floating btn-medium grey, btn tooltipped" data-position="right" data-delay="50" data-tooltip="Cores"><i class="large material-icons"> format_color_fill</i></a><br>
          <a class="btn-floating btn-medium grey, btn tooltipped" data-position="right" data-delay="50" data-tooltip="Fonte Escrita"><i class="large material-icons"> format_italic</i></a><br>
          <a class="btn-floating btn-medium red, btn tooltipped" data-position="right" data-delay="50" data-tooltip="Tamanho Letra"> <i class="large material-icons"> text_fields</i></a><br>
 -->

          <div class="input-field col s6">
         Cores: <input  id="cor" class="jscolor" value="ab2567">
           </div>
</div>
</div>


           <div class="col s12 m4 l8">
            <!-- Teal page content  -->
            <!-- <div class="card horizontal"> -->
              <div id="cvs">
                   <canvas id="myCanvas" class="canvas" width="800" height="500">Seu browser não suporta canvas, é hora de trocar!.</canvas>
              </div>
            <!-- </div> -->
          </div>


        <div class="col  s12 m4 l2">
          <div class="card card-center white">
                  <div class="card-content black-text">
                    <div class="center">
                        <span class="card-title">P/ Escrever:</span>
                    </div>

                          <form name="form"  method="get" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">

                       <div class="input-field col s12">
                          <input id="texto" name="texto" type="text" class="active validate" required>
                          <label for="texto">Texto</label>
                       </div>

                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                        <select name="fonte" id="fonte">
                          <option value="" disabled selected>Choose your option</option>
                          <option value="'Georgia'">Georgia</option>
                          <option value="'Arial'">Arial</option>
                          <option value="'Book Antiqua'">Book Antiqua</option>
                          <option value="'Comic Sans MS'">Comic Sans MS</option>
                        </select>
                        <label>Fontes</label>
                      </div>
                    </div>


                    <div class="row">
                        <div class="input-field col s12">
                        <select name="formatacao" id="formatacao">
                          <option value="" disabled selected>Choose your option</option>
                          <option value="'bold'">Negrito</option>
                          <option value="'italic'">Italico</option>
                        </select>
                        <label>Formatação Fonte</label>
                      </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                        <select name="tam" id="tam">
                          <option value="" disabled selected>Choose your option</option>
                            @for ($i = 8; $i < 36 ; $i++)
                              <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                        <label>Tamanho Fonte</label>
                      </div>
                    </div>


                    <div class="card-action">
                      <div class="center">
                          <input type="button" name="button" Value="formulario" onClick="infEscreve()">

                 </form>
              </div>
        </div>
      </div>

</div>
>



@endsection
