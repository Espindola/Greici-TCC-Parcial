-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 07-Dez-2016 às 21:10
-- Versão do servidor: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `desart`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `dica`
--

CREATE TABLE `dica` (
  `id_dica` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `descricao` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `dica`
--

INSERT INTO `dica` (`id_dica`, `nome`, `descricao`) VALUES
(2, 'Cores Que não podem se usadas de fundo', 'vermelho'),
(3, 'Numero de fontes diferentes', 'deve se usar no máximo 3 fontes diferentes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `lei`
--

CREATE TABLE `lei` (
  `id_lei` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `descricao` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `lei`
--

INSERT INTO `lei` (`id_lei`, `nome`, `descricao`) VALUES
(1, 'Publicidade de Imagem', '.....'),
(2, 'Publicidade de Alimentos', '.......'),
(4, 'Oab', 'No material de escritório e nos cartões sociais poderá constar o nome do advogado, ou o da sociedade, os números de inscrição na OAB, o logotipo e a fotografia do escritório, o horário de atendimento e os idiomas em que o cliente poderá ser atendido (artigo 44 e parágrafo único). Mas está proibido nos cartões de visita a inclusão de fotografias pessoais ou de terceiros, bem como menção a qualquer ');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `padrao_template`
--

CREATE TABLE `padrao_template` (
  `id_padrao` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `tamanho_largura` float NOT NULL,
  `tamanho_altura` float NOT NULL,
  `num_max_fonte` int(11) NOT NULL,
  `tamanho_min_font` int(11) NOT NULL,
  `tamanho_max_font` int(11) NOT NULL,
  `contem` varchar(350) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `padrao_template`
--

INSERT INTO `padrao_template` (`id_padrao`, `nome`, `tamanho_largura`, `tamanho_altura`, `num_max_fonte`, `tamanho_min_font`, `tamanho_max_font`, `contem`) VALUES
(1, 'Cartão de Visitas', 216, 360, 3, 9, 13, 'Logo da Empresa; Nome da Pessoa; Contato; Deve-se deixar a parte de trás do cartão com espaço para poder escrever.'),
(2, 'Banner', 828, 900, 4, 12, 15, 'intodução, desenvolvimente'),
(4, 'Banner', 720, 720, 2, 12, 15, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `template_lei`
--

CREATE TABLE `template_lei` (
  `id_padrao` int(11) NOT NULL,
  `id_lei` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `template_lei`
--

INSERT INTO `template_lei` (`id_padrao`, `id_lei`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(4, 2),
(4, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Greici', 'greicii@gmail.com', '$2y$10$orqwemjoXplgGF4tT7zHvO3qbrqpn2JMQ/AP0beucrVJRmIZsmjCm', 'UuTRO6tgdD0VBVD4fSrz7qUeTZ7An2NIPWyoyfoGKSpZ00jGcVqWw4hIg2Tb', '2016-08-22 22:12:31', '2016-08-23 16:38:13'),
(2, 'Greici', 'greici@gmail.com', '$2y$10$c3aiUWL84xFD28qN7Eg5gOMSyD/VsILQ6m7pZCtdp5ytxCk1.46Z.', NULL, '2016-11-24 01:39:05', '2016-11-24 01:39:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dica`
--
ALTER TABLE `dica`
  ADD PRIMARY KEY (`id_dica`);

--
-- Indexes for table `lei`
--
ALTER TABLE `lei`
  ADD PRIMARY KEY (`id_lei`);

--
-- Indexes for table `padrao_template`
--
ALTER TABLE `padrao_template`
  ADD PRIMARY KEY (`id_padrao`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `template_lei`
--
ALTER TABLE `template_lei`
  ADD KEY `id_lei` (`id_lei`),
  ADD KEY `id_padrao` (`id_padrao`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dica`
--
ALTER TABLE `dica`
  MODIFY `id_dica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lei`
--
ALTER TABLE `lei`
  MODIFY `id_lei` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `padrao_template`
--
ALTER TABLE `padrao_template`
  MODIFY `id_padrao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `template_lei`
--
ALTER TABLE `template_lei`
  ADD CONSTRAINT `template_lei_ibfk_1` FOREIGN KEY (`id_lei`) REFERENCES `lei` (`id_lei`),
  ADD CONSTRAINT `template_lei_ibfk_2` FOREIGN KEY (`id_padrao`) REFERENCES `padrao_template` (`id_padrao`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
